package world.travel.travelquiz.repository.impl

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.database.entities.UserData
import world.travel.travelquiz.repository.UserDataRepository

class UserDataRepositoryImpl(database: AppDatabase) : UserDataRepository {

    private val userDao = database.userDataDao()

    override suspend fun hasUserdata(): Boolean {
        return userDao.get() != null
    }

    override suspend fun insert(userData: UserData) {
        userDao.insert(userData)
    }

    override suspend fun getUserdata(): UserData? {
        return userDao.get()
    }

    override suspend fun update(userData: UserData) {
        userDao.update(userData)
    }

    override suspend fun addScore(score: Int) {
        val userData = userDao.get()!!
        userData.addScore(score)
        userDao.update(userData)
    }

    override fun isSoundOn(): LiveData<Boolean> {
        return userDao.isSoundOn()
    }

    override fun isVibrationOn(): LiveData<Boolean> {
        return userDao.isVibrationOn()
    }

    override fun getScore(): LiveData<Int> {
        return userDao.getScore()
    }

    override suspend fun toggleVibration() {
        val userData = userDao.get()!!
        userData.toggleVibration()
        userDao.update(userData)
    }

    override suspend fun toggleSound() {
        val userData = userDao.get()!!
        userData.toggleSound()
        userDao.update(userData)
    }
}