package world.travel.travelquiz.repository.impl

import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.repository.HintRepository

class HintRepositoryImpl(database: AppDatabase) : HintRepository {

    private val hintDao = database.hintsDao()

    override suspend fun insert(hint: Hint) {
        hintDao.insert(hint)
    }

    override suspend fun findById(id: Int): Hint {
        return hintDao.findById(id)
    }

    override suspend fun getAll(): List<Hint> {
        return hintDao.getAll()
    }

}