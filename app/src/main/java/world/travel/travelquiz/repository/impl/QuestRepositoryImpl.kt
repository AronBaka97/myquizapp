package world.travel.travelquiz.repository.impl

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.repository.QuestRepository

class QuestRepositoryImpl(database: AppDatabase) : QuestRepository {

    private val questDao = database.questDao()

    private val continentDao = database.continentDao()

    private val userDataDao = database.userDataDao()

    override suspend fun insert(quest: Quest) {
        questDao.insert(quest)
        val continent = continentDao.findById(quest.continent)
        continent.totalQuests += 1
        continentDao.update(continent)
    }

    override suspend fun findById(id: Int): Quest {
        return questDao.findById(id)
    }

    override suspend fun getAll(): List<Quest> {
        return questDao.getAll()
    }

    override suspend fun numberOfQuests(continentId: Int): Int {
        return questDao.getNrOfQuests()
    }

    override fun getAllByContinentAndCategory(continentId: Int, category: Int): LiveData<List<Quest>?> {
        return questDao.getAllByContinentAndCategory(continentId, category)
    }

    override suspend fun solve(questId: Int) {
        val quest = questDao.findById(questId)
        quest.solved = true
        questDao.update(quest)

        val continent = continentDao.findById(quest.continent)
        continent.completed += 1
        continentDao.update(continent)
    }

    override suspend fun isSolved(questId: Int): Boolean {
        return questDao.findById(questId).solved
    }

    override suspend fun resetAll() {

        continentDao.resetAll()

        questDao.resetAll()

        val userData = userDataDao.get()!!
        userData.score = 0
        userDataDao.update(userData)
    }

    override suspend fun consumeHint(questId: Int, hint: Hint) {
        val quest = questDao.findById(questId)
        quest.hints.add(hint.id)
        questDao.update(quest)

        val userData = userDataDao.get()!!
        userData.subtractScore(hint.points)
        userDataDao.update(userData)
    }
}