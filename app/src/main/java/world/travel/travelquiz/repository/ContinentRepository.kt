package world.travel.travelquiz.repository

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.entities.Continent

interface ContinentRepository {

    suspend fun insert(continent: Continent)

    suspend fun findById(id: Int): Continent

    fun getAll(): LiveData<List<Continent>?>

    suspend fun getNumberOfContinents(): Int
}