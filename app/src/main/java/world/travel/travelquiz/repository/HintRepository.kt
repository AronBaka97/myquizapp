package world.travel.travelquiz.repository

import world.travel.travelquiz.database.entities.Hint

interface HintRepository {

    suspend fun insert(hint: Hint)

    suspend fun findById(id: Int): Hint

    suspend fun getAll(): List<Hint>
}