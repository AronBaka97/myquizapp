package world.travel.travelquiz.repository

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.entities.UserData

interface UserDataRepository {

    suspend fun hasUserdata(): Boolean

    suspend fun insert(userData: UserData)

    suspend fun getUserdata(): UserData?

    suspend fun update(userData: UserData)

    suspend fun addScore(score: Int)

    fun isSoundOn(): LiveData<Boolean>

    fun isVibrationOn(): LiveData<Boolean>

    fun getScore(): LiveData<Int>

    suspend fun toggleVibration()

    suspend fun toggleSound()

}