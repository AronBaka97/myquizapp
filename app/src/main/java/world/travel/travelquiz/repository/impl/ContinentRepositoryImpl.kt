package world.travel.travelquiz.repository.impl

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.database.entities.Continent
import world.travel.travelquiz.repository.ContinentRepository

class ContinentRepositoryImpl(database: AppDatabase) : ContinentRepository {

    private val continentDao = database.continentDao()

    override suspend fun insert(continent: Continent) {
        continentDao.insert(continent)
    }

    override suspend fun findById(id: Int): Continent {
        return continentDao.findById(id)
    }

    override fun getAll(): LiveData<List<Continent>?> {
        return continentDao.getAll()
    }

    override suspend fun getNumberOfContinents(): Int {
        return continentDao.getNrOfContinents()
    }
}