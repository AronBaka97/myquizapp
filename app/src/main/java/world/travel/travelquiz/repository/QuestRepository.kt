package world.travel.travelquiz.repository

import androidx.lifecycle.LiveData
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.database.entities.Quest

interface QuestRepository {

    suspend fun insert(quest: Quest)

    suspend fun findById(id: Int): Quest

    suspend fun getAll(): List<Quest>

    suspend fun numberOfQuests(continentId: Int): Int

    fun getAllByContinentAndCategory(continentId: Int, category: Int): LiveData<List<Quest>?>

    suspend fun solve(questId: Int)

    suspend fun isSolved(questId: Int): Boolean

    suspend fun resetAll()

    suspend fun consumeHint(questId: Int, hint: Hint)
}