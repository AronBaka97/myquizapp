package world.travel.travelquiz.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class UserData(
    @PrimaryKey
    var id: Int,

    var score: Int,
    var sound: Boolean,
    var vibration: Boolean
) {

    fun addScore(score: Int) {
        this.score += score
    }

    fun subtractScore(score: Int) {
        this.score -= score
    }

    fun toggleSound() {
        sound = !sound
    }

    fun toggleVibration() {
        vibration = !vibration
    }
}