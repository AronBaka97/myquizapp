package world.travel.travelquiz.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Hint(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    val title: String,
    val points: Int
)