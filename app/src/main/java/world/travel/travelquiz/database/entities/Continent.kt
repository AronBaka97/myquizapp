package world.travel.travelquiz.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class Continent(

    @PrimaryKey(autoGenerate = true)
    val id: Int,

    val image: Int,
    val cname: String,
    val color: Int,
    var completed: Int,
    var totalQuests: Int
) : Serializable