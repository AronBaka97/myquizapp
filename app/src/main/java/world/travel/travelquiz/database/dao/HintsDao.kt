package world.travel.travelquiz.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import world.travel.travelquiz.database.entities.Hint

@Dao
interface HintsDao {

    @Insert
    suspend fun insert(hint: Hint)

    @Query("SELECT * FROM hint WHERE id = :id")
    suspend fun findById(id: Int): Hint

    @Query("SELECT * FROM hint")
    suspend fun getAll(): List<Hint>
}