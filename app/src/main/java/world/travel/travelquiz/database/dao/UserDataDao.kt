package world.travel.travelquiz.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import world.travel.travelquiz.database.entities.UserData

@Dao
interface UserDataDao {

    @Update
    suspend fun update(userData: UserData)

    @Query("SELECT * FROM UserData")
    suspend fun get(): UserData?

    @Query("SELECT sound FROM userdata")
    fun isSoundOn(): LiveData<Boolean>

    @Query("SELECT vibration FROM userdata")
    fun isVibrationOn(): LiveData<Boolean>

    @Query("SELECT score FROM userdata")
    fun getScore(): LiveData<Int>

    @Insert
    suspend fun insert(userData: UserData)

}