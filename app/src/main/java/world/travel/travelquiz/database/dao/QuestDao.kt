package world.travel.travelquiz.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import world.travel.travelquiz.database.entities.Quest

@Dao
interface QuestDao {

    @Insert
    suspend fun insert(quest: Quest)

    @Query("SELECT * FROM quest WHERE continent = :continent AND category = :category")
    fun getAllByContinentAndCategory(continent: Int, category: Int): LiveData<List<Quest>?>

    @Query("SELECT COUNT(*) FROM quest")
    suspend fun getNrOfQuests(): Int

    @Query("SELECT * FROM quest")
    suspend fun getAll(): List<Quest>

    @Update
    suspend fun update(quest: Quest)

    @Query("SELECT * FROM quest WHERE id = :id")
    suspend fun findById(id: Int): Quest

    @Query("UPDATE quest SET solved = 0, hints='[]'")
    suspend fun resetAll()
}