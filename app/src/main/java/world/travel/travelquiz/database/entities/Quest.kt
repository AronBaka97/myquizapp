package world.travel.travelquiz.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class Quest(
    @PrimaryKey(autoGenerate = true)
    val id: Int,

    val image: Int,
    val cname: String,
    val continent: Int,
    val difficulty: Int,
    var favourite: Int,
    var solved: Boolean,
    val category: Int,

    val hints: MutableList<Int>
) : Serializable