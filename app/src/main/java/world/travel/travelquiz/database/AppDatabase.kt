package world.travel.travelquiz.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import world.travel.travelquiz.database.converters.HintListConverter
import world.travel.travelquiz.database.dao.ContinentDao
import world.travel.travelquiz.database.dao.HintsDao
import world.travel.travelquiz.database.dao.QuestDao
import world.travel.travelquiz.database.dao.UserDataDao
import world.travel.travelquiz.database.entities.Continent
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.database.entities.UserData

@Database(entities = [Quest::class, Continent::class, UserData::class, Hint::class], version = 1)
@TypeConverters(HintListConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun questDao(): QuestDao

    abstract fun continentDao(): ContinentDao

    abstract fun userDataDao(): UserDataDao

    abstract fun hintsDao(): HintsDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "quiz_db"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}