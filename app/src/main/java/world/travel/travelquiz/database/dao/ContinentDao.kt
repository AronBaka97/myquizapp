package world.travel.travelquiz.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import world.travel.travelquiz.database.entities.Continent

@Dao
interface ContinentDao {

    @Insert
    suspend fun insert(continent: Continent)

    @Query("SELECT * FROM continent WHERE id = :id")
    suspend fun findById(id: Int): Continent

    @Update
    suspend fun update(continent: Continent)

    @Query("SELECT * FROM continent")
    fun getAll(): LiveData<List<Continent>?>

    @Query("SELECT COUNT(*) FROM continent")
    suspend fun getNrOfContinents(): Int

    @Query("UPDATE continent SET completed = 0")
    suspend fun resetAll()
}