package world.travel.travelquiz.activities.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import world.travel.travelquiz.repository.ContinentRepository
import world.travel.travelquiz.repository.QuestRepository
import world.travel.travelquiz.repository.UserDataRepository
import world.travel.travelquiz.util.Event

class HomeViewModel(
    private val userdataDataRepository: UserDataRepository,
    private val questRepository: QuestRepository,
    continentRepository: ContinentRepository
) : ViewModel() {

    val soundOn = userdataDataRepository.isSoundOn()

    val vibrateOn = userdataDataRepository.isVibrationOn()

    val continents =  continentRepository.getAll()

    private val _resetQuizPressed = MutableLiveData<Event<Boolean>>()
    val resetQuizPressed: LiveData<Event<Boolean>> = _resetQuizPressed

    fun toggleSound() {
        viewModelScope.launch {
            userdataDataRepository.toggleSound()
        }
    }

    fun toggleVibration() {
        viewModelScope.launch {
            userdataDataRepository.toggleVibration()
        }
    }

    fun resetQuiz() {
        _resetQuizPressed.value = Event(true)
    }

    fun resetAll() {
        viewModelScope.launch {
            questRepository.resetAll()
        }
    }
}