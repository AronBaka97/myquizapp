package world.travel.travelquiz.activities.start

interface StartNavigator {

    fun onGoToMainApplication()
}