package world.travel.travelquiz.activities.start

import android.content.Intent
import android.os.Bundle
import world.travel.travelquiz.R
import world.travel.travelquiz.activities.main.MainActivity
import world.travel.travelquiz.base.BaseActivity

class StartActivity : BaseActivity(), StartNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, StartFragment.newInstance())
                .commitNow()
        }
    }

    override fun onGoToMainApplication() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}