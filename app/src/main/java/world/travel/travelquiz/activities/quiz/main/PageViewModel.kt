package world.travel.travelquiz.activities.quiz.main

import androidx.lifecycle.*
import kotlinx.coroutines.launch
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.repository.QuestRepository

class PageViewModel(private val questRepository: QuestRepository) : ViewModel() {

    private val _index = MutableLiveData<Int>()

    private var _continentId: Int = 0

    var quests = questRepository.getAllByContinentAndCategory(_continentId, 0)

    val category = _index.map {
        QUIZ_CATEGORIES[it - 1]
    }

    val icon = _index.map {
        QUIZ_ICONS[it - 1]
    }

    fun setContinentId(continentId: Int) {
        _continentId = continentId
    }

    fun setIndex(index: Int) {
        _index.value = index
        getQuestsFromCategory(index)
    }

    private fun getQuestsFromCategory(category: Int) {
        quests = questRepository.getAllByContinentAndCategory(_continentId, category)
    }
}