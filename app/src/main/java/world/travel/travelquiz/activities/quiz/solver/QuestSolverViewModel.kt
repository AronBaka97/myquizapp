package world.travel.travelquiz.activities.quiz.solver

import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.repository.HintRepository
import world.travel.travelquiz.repository.QuestRepository
import world.travel.travelquiz.repository.UserDataRepository
import java.util.*

class QuestSolverViewModel(private val quest: Quest,
                           private val questRepository: QuestRepository,
                           private val userDataRepository: UserDataRepository,
                           private val hintRepository: HintRepository
) : ViewModel() {

    val solutionWithExtraChars = quest.cname.toUpperCase(Locale.ROOT).split(";")[0]

    private val solution = solutionWithExtraChars.replace("/", "")

    val options = shuffle(clear(quest.cname.toUpperCase(Locale.ROOT)))

    val score = userDataRepository.getScore()

    val image = quest.image

    private val _selectedLetters = MutableLiveData<MutableList<String?>>()
    val selectedLetters: LiveData<MutableList<String?>> = _selectedLetters

    private val _availableLetters = MutableLiveData<MutableList<String?>>()
    val availableLetters: LiveData<MutableList<String?>> = _availableLetters

    private val _highlightedButtonIndices = MutableLiveData<MutableList<Int>>()
    val highlightedButtonIndices: LiveData<MutableList<Int>> = _highlightedButtonIndices

    private val _correctSolution = MutableLiveData<Boolean>()
    val correctSolution: LiveData<Boolean> = _correctSolution

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message

    private val _hint = MutableLiveData<Hint>()
    val hint: LiveData<Hint> = _hint

    private lateinit var hints: List<Hint>

    private var positionToJump = -1

    init {
        val selectedLettersList = ArrayList<String?>()
        val availableLettersList = ArrayList<String?>()
        val highlightedButtonList = ArrayList<Int>()
        for (i in solution) {
            selectedLettersList.add(null)
            highlightedButtonList.add(0)
        }
        for (i in options) {
            availableLettersList.add("$i")
        }
        update(selectedLettersList, availableLettersList, highlightedButtonList)
        viewModelScope.launch {
            hintRepository.getAll().let {
                hints = it
            }
        }
    }

    fun onHintSelected(position: Int) {
        _hint.value = hints[position]
    }

    suspend fun isHintConsumed(hintId: Int): Boolean {
        val questFromDb = questRepository.findById(quest.id)
        return questFromDb.hints.contains(hintId)
    }

    fun consumeHint(hint: Hint) {
        viewModelScope.launch {
            questRepository.consumeHint(quest.id, hint)
        }
    }

    private fun update(selectedLettersList: MutableList<String?>, availableLettersList: MutableList<String?>, highlightedButtonList: MutableList<Int>) {
        _selectedLetters.value = selectedLettersList
        _availableLetters.value = availableLettersList
        _highlightedButtonIndices.value = highlightedButtonList
        var guessedSolution = ""
        for(letter in selectedLettersList){
            guessedSolution += letter
        }
        if(guessedSolution.length == solution.length){
            if(guessedSolution == solution) {
                _correctSolution.value = true
                _message.value = "Congratulations!"
                viewModelScope.launch {
                    questRepository.solve(quest.id)
                    userDataRepository.addScore(quest.difficulty)
                }
            } else if(difference(solution, guessedSolution) <= 2 && solution.length > 5 && solution.length == guessedSolution.length) {
                _message.value = "Almost there, Try again!"
            } else {
                _message.value = "Wrong answer! :("
            }
        }
    }

    private fun difference(string1: String, string2: String): Int {
        var difLetters = 0
        for (i in string1.indices) {
            if (string1[i] != string2[i]) {
                difLetters++
            }
        }
        return difLetters
    }

    @Suppress("JavaCollectionsStaticMethodOnImmutableList")
    private fun shuffle(text: String): String {
        val letters = text.split("")
        Collections.shuffle(letters)
        var shuffled = ""
        for (letter in letters) {
            shuffled += letter
        }
        return shuffled
    }

    private fun clear(text: String): String {
        return text
            .replace(";", "")
            .replace(" ", "")
            .replace("-", "")
            .replace("/", "")
    }

    fun handleLetterDeselected(button: Button) {
        val position = button.id
        val availableLettersList = availableLetters.value!!
        val selectedLettersList = selectedLetters.value!!
        val highlightedButtonList = highlightedButtonIndices.value!!

        if (button.text.isNotBlank()) {
            var i = 0
            while ("${options[i]}" != button.text || availableLettersList[i] != null) {
                i++
            }
            availableLettersList[i] = "${button.text}"
            selectedLettersList[position] = null
        } else {
            if (position == positionToJump) {
                highlightedButtonList[positionToJump] = 0
                positionToJump = -1
            } else {
                if (positionToJump != -1) {
                    highlightedButtonList[positionToJump] = 0
                }
                positionToJump = position
                highlightedButtonList[position] = 1
            }
        }
        update(selectedLettersList, availableLettersList, highlightedButtonList)
    }

    fun handleLetterSelected(position: Int, button: Button) {
        try {
            val availableLettersList = availableLetters.value!!
            val selectedLettersList = selectedLetters.value!!
            val highlightedButtonList = highlightedButtonIndices.value!!

            if (positionToJump == -1) {
                val firstNull = selectedLettersList.indexOfFirst { it == null }
                selectedLettersList[firstNull] = "${button.text}"
            } else {
                selectedLettersList[positionToJump] = "${button.text}"
                highlightedButtonList[positionToJump] = 0
                positionToJump = -1
            }
            availableLettersList[position] = null
            update(selectedLettersList, availableLettersList, highlightedButtonList)
        } catch (e: IndexOutOfBoundsException) {
            // do nothing
        }
    }

    suspend fun isQuestSolved(questId: Int): Boolean {
        return questRepository.isSolved(questId)
    }
}