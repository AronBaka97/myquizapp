package world.travel.travelquiz.activities.quiz.solver

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch
import world.travel.travelquiz.R
import world.travel.travelquiz.base.BaseFragment
import world.travel.travelquiz.database.entities.Hint
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.databinding.FragmentQuestSolverBinding
import world.travel.travelquiz.util.factory.media.MediaPlayerFactory
import world.travel.travelquiz.util.factory.media.MediaPlayerFactoryImpl
import world.travel.travelquiz.util.factory.vibration.VibrationFactory
import world.travel.travelquiz.util.factory.vibration.VibrationFactoryImpl
import world.travel.travelquiz.util.factory.viewmodels.QuestSolverVMFactory
import java.util.*


class QuestSolverFragment(private val quest: Quest) :
    BaseFragment<QuestSolverViewModel, FragmentQuestSolverBinding>(
        R.layout.fragment_quest_solver
    ) {

    private lateinit var optionsLayout: TableLayout

    private lateinit var solutionLayout: TableLayout

    override val viewModel: QuestSolverViewModel by activityViewModels {
        QuestSolverVMFactory(
            requireContext(),
            quest
        )
    }

    private lateinit var vibrationFactory: VibrationFactory

    private lateinit var mediaPlayerFactory: MediaPlayerFactory

    companion object {
        fun newInstance(quest: Quest) = QuestSolverFragment(quest)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding?.viewModel = viewModel
        optionsLayout = binding?.solutionOptions!!
        solutionLayout = binding?.solutionGuessing!!
        vibrationFactory = VibrationFactoryImpl(requireContext())
        mediaPlayerFactory = MediaPlayerFactoryImpl(requireContext())
        buildView()
    }

    private fun setupObservers() {

        viewModel.correctSolution.observe(viewLifecycleOwner, {
            if (it) {
                onTaskSolved()
            }
        })

        viewModel.hint.observe(viewLifecycleOwner, {
            viewLifecycleOwner.lifecycleScope.launch {
                when {
                    viewModel.isHintConsumed(it.id) -> {
                        showHint(it)
                    }
                    viewModel.score.value!! >= it.points -> {
                        askUseHint(it)
                    }
                    else -> {
                        showNotEnoughPointsDialog()
                    }
                }
            }
        })

        viewModel.message.observe(viewLifecycleOwner, {
            displayCenterAlignedToast(it)
        })

        viewModel.selectedLetters.observe(viewLifecycleOwner) {
            repaintUI()
        }

        viewModel.availableLetters.observe(viewLifecycleOwner) {
            repaintUI()
        }

        viewModel.highlightedButtonIndices.observe(viewLifecycleOwner) {
            repaintUI()
        }
    }

    private fun showNotEnoughPointsDialog() {
        AlertDialog.Builder(context)
            .setTitle("Hint Info")
            .setMessage("You don't have enough points to use this hint :(")
            .setPositiveButton("ok") { _, _ -> }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun showHint(hint: Hint) {
        AlertDialog.Builder(context)
            .setTitle("Hint")
            .setMessage("${hint.points}:D?")
            .setPositiveButton("ok") { _, _ -> }
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun askUseHint(hint: Hint) {
        AlertDialog.Builder(context)
            .setTitle("Use Hint")
            .setMessage("Are you sure you want to use hint for ${hint.points} points?")
            .setPositiveButton(android.R.string.yes) { _, _ ->
                viewModel.consumeHint(hint)
                showHint(hint)
            }
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun repaintUI() {
        drawSolutionLayout()
        drawOptionsLayout()
    }

    private fun drawSolutionLayout() {
        solutionLayout.removeAllViews()
        val selectedLetters = viewModel.selectedLetters.value!!
        val highlightedButtonIndices = viewModel.highlightedButtonIndices.value!!
        val rows = splitSolutionToTableRows(
            viewModel.solutionWithExtraChars,
            selectedLetters,
            highlightedButtonIndices
        )
        addToView(rows, solutionLayout)
    }

    private fun splitSolutionToTableRows(
        solution: String,
        selectedLetters: MutableList<String?>,
        highlightedButtonIndices: MutableList<Int>
    ): MutableList<TableRow> {
        val rows: MutableList<TableRow> = arrayListOf()
        val lines = solution.split("/")
        var selectedLettersPosition = 0
        for (fragment in lines) {
            val tableRow = createCenterAlignedTableRow()
            for (i in fragment) {
                val solutionCharLayout = View.inflate(
                    requireContext(),
                    R.layout.solution_button,
                    null
                )
                val solutionButton = solutionCharLayout!!.findViewById<Button>(R.id.solutionChar)
                if (i !in 'A'..'Z') {
                    selectedLetters[selectedLettersPosition] = "$i"
                    solutionButton.setBackgroundColor(Color.WHITE)
                    solutionButton.text = "$i"
                } else {
                    if (selectedLetters[selectedLettersPosition] == null) {
                        if (highlightedButtonIndices[selectedLettersPosition] == 1) {
                            applyStyleOnButton(solutionButton, Color.YELLOW, Color.BLACK)
                        } else {
                            applyStyleOnButton(solutionButton, Color.BLACK, Color.BLACK)
                        }
                    } else {
                        applyStyleOnButton(
                            solutionButton,
                            ContextCompat.getColor(requireContext(), R.color.button_bg),
                            ContextCompat.getColor(requireContext(), R.color.button_bg)
                        )
                        solutionButton.text = selectedLetters[selectedLettersPosition]
                    }
                    solutionButton.id = selectedLettersPosition
                    solutionButton.setOnClickListener {
                        viewModel.handleLetterDeselected(solutionButton)
                    }
                }
                if (!("$i" == " " && tableRow.childCount == 0)) {
                    tableRow.addView(solutionCharLayout)
                }
                selectedLettersPosition++
            }
            if (fragment != lines[lines.size - 1]) {
                addNextLineIndicator(tableRow)
            }
            rows.add(tableRow)
        }
        return rows
    }

    private fun createCenterAlignedTableRow(): TableRow {
        val tableRow = TableRow(requireContext())
        tableRow.gravity = Gravity.CENTER
        return tableRow
    }

    private fun applyStyleOnButton(button: Button, borderColor: Int, backgroundColor: Int) {
        val drawable = button.background as GradientDrawable
        drawable.setColor(backgroundColor)
        drawable.setStroke(5, borderColor)
    }

    private fun addNextLineIndicator(tableRow: TableRow) {
        val solutionCharLayout = View.inflate(requireContext(), R.layout.solution_button, null)
        val nextLineIndicator = solutionCharLayout!!.findViewById<Button>(R.id.solutionChar)
        nextLineIndicator.setBackgroundResource(R.drawable.next_line_indicator)
        tableRow.addView(solutionCharLayout)
    }

    private fun addToView(buttonRowList: MutableList<TableRow>, destLayout: TableLayout) {
        for (tableRow in buttonRowList) {
            destLayout.addView(tableRow)
        }
    }

    private fun drawOptionsLayout() {
        optionsLayout.removeAllViews()
        val rows: MutableList<TableRow> = ArrayList()
        val options = viewModel.options
        val availableLetters = viewModel.availableLetters.value!!

        for (i in options.indices) {
            if (i % 8 == 0) {
                rows.add(TableRow(requireContext()))
            }
            val solutionCharLayout = View.inflate(requireContext(), R.layout.solution_button, null)
            if (options[i] in 'A'..'Z') {
                val solutionButton = solutionCharLayout!!.findViewById<Button>(R.id.solutionChar)
                if (availableLetters[i] != null) {
                    solutionButton.text = "${options[i]}"
                    solutionButton.setOnClickListener {
                        viewModel.handleLetterSelected(i, solutionButton)
                        viewLifecycleOwner.lifecycleScope.launch {
                            vibrationFactory.getVibrationType().vibrate()
                        }
                    }
                    applyStyleOnButton(
                        solutionButton,
                        ContextCompat.getColor(requireContext(), R.color.button_bg),
                        ContextCompat.getColor(requireContext(), R.color.button_bg)
                    )
                } else {
                    solutionButton.visibility = View.INVISIBLE
                }
                rows[rows.size - 1].addView(solutionCharLayout)
            }
        }
        addToView(rows, optionsLayout)
    }

    private fun onTaskSolved() {
        optionsLayout.removeAllViews()
        solutionLayout.removeAllViews()
        viewLifecycleOwner.lifecycleScope.launch {
            mediaPlayerFactory.getMediaPlayer().playTone()
        }
        //navigate to solution fragment
    }

    private fun buildView() {
        viewLifecycleOwner.lifecycleScope.launch {
            if (!viewModel.isQuestSolved(quest.id)) {
                setupObservers()
            } else {
                //navigate to solution fragment
                println("SOLVED")
            }
        }
    }

    private fun displayCenterAlignedToast(message: String) {
        val toast = Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }
}