package world.travel.travelquiz.activities.quiz.solver

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import world.travel.travelquiz.R
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.util.factory.viewmodels.QuestSolverVMFactory

class QuestSolverActivity : AppCompatActivity() {

    private var menu: Menu? = null

    private lateinit var viewModel: QuestSolverViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quest_solver)
        val quest = intent.getSerializableExtra("quest") as Quest
        viewModel = ViewModelProvider(this, QuestSolverVMFactory(this, quest)).get(QuestSolverViewModel::class.java)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, QuestSolverFragment.newInstance(quest))
                .commitNow()
        }
        setupToolbar()
        setupObservers()
    }

    private fun setupObservers() {

        viewModel.score.observe(this, {
            val menuItem: MenuItem? = menu?.findItem(R.id.nrOfHints)
            menuItem?.title = "$it x"
        })

    }

    private fun setupToolbar() {
        title = null
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(intent.getStringExtra("bgColor")!!.toInt()))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        this.menu = menu
        val inflater = menuInflater
        inflater.inflate(R.menu.hints_menu, menu)
        val menuItem: MenuItem? = menu.findItem(R.id.nrOfHints)
        menuItem?.title = "${viewModel.score.value ?: 0} x"
        return true
    }
}