package world.travel.travelquiz.activities.main

import world.travel.travelquiz.database.entities.Continent

interface MainNavigator {

    fun onContinentSelected(continent: Continent)
}