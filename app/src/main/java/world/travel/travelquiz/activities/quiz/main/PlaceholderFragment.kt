package world.travel.travelquiz.activities.quiz.main

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import world.travel.travelquiz.R
import world.travel.travelquiz.base.BaseFragment
import world.travel.travelquiz.database.entities.Continent
import world.travel.travelquiz.databinding.FragmentQuestSelectorBinding
import world.travel.travelquiz.util.factory.viewmodels.QuestsVMFactory


class PlaceholderFragment : BaseFragment<PageViewModel, FragmentQuestSelectorBinding>(R.layout.fragment_quest_selector) {

    override lateinit var viewModel: PageViewModel

    private lateinit var questsAdapter: QuestListAdapter

    private var navigator: QuestListNavigator? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding?.viewModel = viewModel
        setupObservers()
        setupAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, QuestsVMFactory(requireContext())).get(PageViewModel::class.java).apply {
            setContinentId(arguments?.getInt(ARG_CONTINENT_ID) ?: 0)
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        private const val ARG_CONTINENT_ID = "continent_id"

        @JvmStatic
        fun newInstance(sectionNumber: Int, continent: Continent): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                    putInt(ARG_CONTINENT_ID, continent.id)
                }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            navigator = context as QuestListNavigator
        } catch (e : ClassCastException) {
            println("Activity should implement QuestListNavigator")
        }
    }

    private fun setupObservers() {

        viewModel.quests.observe(viewLifecycleOwner, {
            questsAdapter.notifyDataSetChanged()
        })
    }

    private fun setupAdapter() {
        val recyclerView = binding?.questList
        recyclerView?.layoutManager = GridLayoutManager(requireContext(), 3)
        questsAdapter = QuestListAdapter(requireContext(), viewModel.quests) { navigator?.onGoToSolveQuest(it) }

        val spacing = 14

        recyclerView?.adapter = questsAdapter
        recyclerView?.setPadding(spacing, spacing, spacing, spacing)
        recyclerView?.clipToPadding = false
        recyclerView?.clipChildren = false
        recyclerView?.addItemDecoration(object : ItemDecoration() {
            override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                outRect.set(spacing, spacing, spacing, spacing)
            }
        })
    }
}