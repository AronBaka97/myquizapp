package world.travel.travelquiz.activities.main.home

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import world.travel.travelquiz.R
import world.travel.travelquiz.activities.main.MainNavigator
import world.travel.travelquiz.base.BaseFragment
import world.travel.travelquiz.databinding.FragmentHomeBinding
import world.travel.travelquiz.util.factory.viewmodels.HomeVMFactory


class HomeFragment : BaseFragment<HomeViewModel, FragmentHomeBinding>(R.layout.fragment_home) {

    override val viewModel: HomeViewModel by activityViewModels { HomeVMFactory(requireContext()) }

    private var mainNavigator: MainNavigator? = null

    private lateinit var continentsListAdapter: ContinentsListAdapter

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding?.viewModel = viewModel
        setupAdapter()
        setupObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            mainNavigator = context as MainNavigator
        } catch (e: ClassCastException) {
            println("Activity should implement MainNavigator")
        }
    }

    private fun setupObservers() {

        viewModel.resetQuizPressed.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let {
                showResetDialog()
            }
        })

        viewModel.continents.observe(viewLifecycleOwner, {
            continentsListAdapter.notifyDataSetChanged()
        })

    }

    private fun setupAdapter() {
        val recyclerView = binding?.listContinents
        recyclerView?.layoutManager = LinearLayoutManager(requireContext())
        continentsListAdapter = ContinentsListAdapter(viewModel.continents) {
            mainNavigator?.onContinentSelected(it)
        }
        recyclerView?.adapter = continentsListAdapter
    }

    private fun showResetDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setMessage("Are you sure you want to reset all the solutions?")
            .setNegativeButton("No", null)
            .setPositiveButton(
                "Yes"
            ) { _, _ ->
                viewModel.resetAll()
                Toast.makeText(requireContext(), "Everything reset successfully", Toast.LENGTH_SHORT).show()
            }
        val alert = builder.create()
        alert.show()
    }
}