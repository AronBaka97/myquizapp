package world.travel.travelquiz.activities.quiz.main

import world.travel.travelquiz.database.entities.Quest

interface QuestListNavigator {

    fun onGoToSolveQuest(quest: Quest)
}