package world.travel.travelquiz.activities.quiz.main

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import world.travel.travelquiz.R
import world.travel.travelquiz.activities.main.MainActivity
import world.travel.travelquiz.activities.quiz.solver.QuestSolverActivity
import world.travel.travelquiz.database.entities.Continent
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.util.color.ColorUtils
import world.travel.travelquiz.util.factory.viewmodels.QuestsVMFactory

class QuestSelectorActivity : AppCompatActivity(), QuestListNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz_selector)
        val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, intent.getSerializableExtra("continent") as Continent)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        setupToolbar()
    }

    private fun setupToolbar() {
        val selectedContinent = intent.getSerializableExtra("continent") as Continent
        val colorUtils = ColorUtils()

        val appBar = findViewById<AppBarLayout>(R.id.appbar)
        appBar.apply {
            setBackgroundColor(selectedContinent.color)
        }

        val tabs = findViewById<TabLayout>(R.id.tabs)
        tabs.apply {
            setBackgroundColor(selectedContinent.color)
            setSelectedTabIndicatorColor(colorUtils.getComplementaryColor(selectedContinent.color))
            setSelectedTabIndicatorHeight(10)
        }

        val title = findViewById<TextView>(R.id.title)
        title.text = selectedContinent.cname

        val activityLayout = findViewById<ViewPager>(R.id.view_pager)
        activityLayout.setBackgroundColor(colorUtils.lightenColor(selectedContinent.color, 0.2f))
    }

    override fun onGoToSolveQuest(quest: Quest) {
        val intent = Intent(this, QuestSolverActivity::class.java)
        intent.putExtra("quest", quest)

        val colorUtils = ColorUtils()
        intent.putExtra("bgColor", "${colorUtils.getDifficultyColor(this, quest.difficulty)}")
        startActivity(intent)
    }
}