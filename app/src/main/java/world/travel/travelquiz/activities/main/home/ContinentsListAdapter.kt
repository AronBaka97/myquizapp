package world.travel.travelquiz.activities.main.home

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import world.travel.travelquiz.R
import world.travel.travelquiz.database.entities.Continent

class ContinentsListAdapter (private val continentsList: LiveData<List<Continent>?>, private val continentSelectionListener: (continent: Continent) -> Unit) : RecyclerView.Adapter<ContinentsListAdapter.MyViewHolder>() {

    inner class MyViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val thumbnail = itemView.findViewById<ImageView>(R.id.continentImage)!!
        val continentName = itemView.findViewById<TextView>(R.id.continentName)!!
        val progressBar = itemView.findViewById<ProgressBar>(R.id.continentProgress)!!
        val percentage = itemView.findViewById<TextView>(R.id.progressPercentage)!!
        val score = itemView.findViewById<TextView>(R.id.continentScore)!!
        val continentContainer = itemView.findViewById<RelativeLayout>(R.id.continent_container)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val favouriteView = inflater.inflate(R.layout.continent, parent, false)
        return MyViewHolder(favouriteView)
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        val continent: Continent = continentsList.value!![position]
        viewHolder.thumbnail.setImageResource(continent.image)
        viewHolder.continentName.text = continent.cname

        val completed = continent.completed
        val total = continent.totalQuests
        val ratio: Int = 100 * completed / total

        viewHolder.progressBar.progress = ratio
        viewHolder.score.text = "$completed/$total"
        viewHolder.percentage.text = "$ratio%"

        val drawable = viewHolder.continentContainer.background as GradientDrawable
        drawable.setColor(continent.color)

        viewHolder.continentContainer.setOnClickListener {
            continentSelectionListener.invoke(continent)
        }
    }

    override fun getItemCount(): Int {
        return continentsList.value?.size ?: 0
    }
}