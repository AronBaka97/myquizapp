package world.travel.travelquiz.activities.quiz.main

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import world.travel.travelquiz.R
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.util.color.ColorUtils


class QuestListAdapter(
    private val context: Context, private val questList: LiveData<List<Quest>?>,
    private val questSelectionListener: (questId: Quest) -> Unit
) : RecyclerView.Adapter<QuestListAdapter.MyViewHolder>() {

    inner class MyViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        val thumbnail = itemView.findViewById<ImageView>(R.id.quest_thumb)!!
        val thumbLayout = itemView.findViewById<ConstraintLayout>(R.id.layout_thumb)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        val thumbnailView = inflater.inflate(R.layout.quest_thumbnail, parent, false)
        return MyViewHolder(thumbnailView)
    }

    override fun onBindViewHolder(viewHolder: MyViewHolder, position: Int) {
        val quest: Quest = questList.value!![position]

        Glide.with(viewHolder.itemView.context)
            .load(quest.image)
            .apply(RequestOptions().placeholder(R.drawable.ic_launcher_background).centerCrop())
            .into(viewHolder.thumbnail)

        val colorUtils = ColorUtils()
        if(!quest.solved) {
            viewHolder.thumbnail.colorFilter = colorUtils.grayScaleFilter()
        } else {
            viewHolder.thumbnail.colorFilter = null
        }

        val border = GradientDrawable()
        border.setStroke(12, colorUtils.getDifficultyColor(context, quest.difficulty))
        border.cornerRadius = 15f
        viewHolder.thumbLayout.background = border

        viewHolder.thumbLayout.setOnClickListener {
            questSelectionListener.invoke(quest)
        }
    }

    override fun getItemCount(): Int {
        return questList.value?.size ?: 0
    }
}