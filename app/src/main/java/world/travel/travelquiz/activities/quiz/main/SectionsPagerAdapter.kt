package world.travel.travelquiz.activities.quiz.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import world.travel.travelquiz.R
import world.travel.travelquiz.database.entities.Continent

val QUIZ_CATEGORIES = arrayOf(
    "Cities",
    "Nature",
    "Landmarks"
)

val QUIZ_ICONS = arrayOf(
    R.drawable.city_day,
    R.drawable.nat_wonder,
    R.drawable.landmark
)

class SectionsPagerAdapter(fm: FragmentManager, private val continent: Continent) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return PlaceholderFragment.newInstance(position + 1, continent)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return null
    }

    override fun getCount(): Int {
        return QUIZ_CATEGORIES.size
    }
}