package world.travel.travelquiz.activities.start

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.activityViewModels
import world.travel.travelquiz.R
import world.travel.travelquiz.base.BaseFragment
import world.travel.travelquiz.databinding.FragmentStartBinding
import world.travel.travelquiz.util.factory.viewmodels.StartVMFactory

class StartFragment : BaseFragment<StartViewModel, FragmentStartBinding>(R.layout.fragment_start) {

    override val viewModel: StartViewModel by activityViewModels { StartVMFactory(requireContext()) }

    private var startNavigator: StartNavigator? = null

    companion object {
        fun newInstance() = StartFragment()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding?.viewModel = viewModel
        setupObservers()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            startNavigator = context as StartNavigator
        } catch (e : ClassCastException) {
            println("Activity should implement StartNavigator")
        }
    }

    override fun onDetach() {
        super.onDetach()
        startNavigator = null
    }

    private fun setupObservers() {

        viewModel.dataLoadingFinished.observe(viewLifecycleOwner, {
            startNavigator?.onGoToMainApplication()
        })

    }

}