package world.travel.travelquiz.activities.start

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import world.travel.travelquiz.database.entities.UserData
import world.travel.travelquiz.repository.ContinentRepository
import world.travel.travelquiz.repository.HintRepository
import world.travel.travelquiz.repository.QuestRepository
import world.travel.travelquiz.repository.UserDataRepository
import world.travel.travelquiz.util.builder.CityListBuilder
import world.travel.travelquiz.util.builder.ContinentListBuilder
import world.travel.travelquiz.util.builder.HintsListBuilder

class StartViewModel(
    private val questRepository: QuestRepository,
    private val continentRepository: ContinentRepository,
    private val userDataRepository: UserDataRepository,
    private val hintRepository: HintRepository
) : ViewModel() {

    private val _dataLoadingFinished = MutableLiveData<Boolean>()
    val dataLoadingFinished: LiveData<Boolean> = _dataLoadingFinished

    init {
        viewModelScope.launch {
            insertValuesIntoDB()
        }
    }

    private suspend fun insertValuesIntoDB() {

        //userdata
        if (!userDataRepository.hasUserdata()) {
            userDataRepository.insert(UserData(0, 0, sound = true, vibration = true))
        }

        //continents
        if (continentRepository.getNumberOfContinents() == 0) {
            val continents = ContinentListBuilder.getContinentList()
            for (continent in continents) {
                continentRepository.insert(continent)
            }
        }

        //hints
        if (hintRepository.getAll().isEmpty()) {
            for (hint in HintsListBuilder.generateHintsList()) {
                hintRepository.insert(hint)
            }
        }

        //quests
        if (questRepository.numberOfQuests(1) == 0) {
            for (city in CityListBuilder.generateEuropeanCities()) {
                questRepository.insert(city)
            }

            for (city in CityListBuilder.generateNorthAmericanCities()) {
                questRepository.insert(city)
            }

            for (city in CityListBuilder.generateSouthAmericanCities()) {
                questRepository.insert(city)
            }

            for (city in CityListBuilder.generateAsianCities()) {
                questRepository.insert(city)
            }

            for (city in CityListBuilder.generateAfricanCities()) {
                questRepository.insert(city)
            }

            for (city in CityListBuilder.generateAustralianCities()) {
                questRepository.insert(city)
            }
        }

        _dataLoadingFinished.value = true
    }
}