package world.travel.travelquiz.activities.main

import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import world.travel.travelquiz.R
import world.travel.travelquiz.R.id.nav_host_fragment
import world.travel.travelquiz.R.id.nav_view
import world.travel.travelquiz.R.layout.activity_main
import world.travel.travelquiz.activities.quiz.main.QuestSelectorActivity
import world.travel.travelquiz.base.BaseActivity
import world.travel.travelquiz.database.entities.Continent


class MainActivity : BaseActivity(), MainNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        draw()
    }

    private fun draw() {
        setContentView(activity_main)
        val navView: BottomNavigationView = findViewById(nav_view)

        val navController = findNavController(nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
                R.id.navigation_statistics,
                R.id.navigation_favourites,
                R.id.navigation_shop
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onContinentSelected(continent: Continent) {
        val intent = Intent(this, QuestSelectorActivity::class.java)
        intent.putExtra("continent", continent)
        startActivity(intent)
    }

}