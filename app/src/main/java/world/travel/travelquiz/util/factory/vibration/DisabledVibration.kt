package world.travel.travelquiz.util.factory.vibration

import android.content.Context

class DisabledVibration(val context: Context): ButtonVibrator {

    override fun vibrate() {
        val vibe = context.getSystemService(Context.VIBRATOR_SERVICE) as android.os.Vibrator
        vibe.vibrate(0)
    }
}