package world.travel.travelquiz.util.factory.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import world.travel.travelquiz.activities.main.home.HomeViewModel
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.repository.impl.ContinentRepositoryImpl
import world.travel.travelquiz.repository.impl.QuestRepositoryImpl
import world.travel.travelquiz.repository.impl.UserDataRepositoryImpl

class HomeVMFactory(private val context: Context) : ViewModelProvider.Factory {

    private val appDataBase = AppDatabase.getDatabase(context)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(UserDataRepositoryImpl(appDataBase),
                            QuestRepositoryImpl(appDataBase),
                            ContinentRepositoryImpl(appDataBase)) as T
    }
}