package world.travel.travelquiz.util.factory.vibration

import android.content.Context
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.repository.impl.UserDataRepositoryImpl

class VibrationFactoryImpl(val context: Context): VibrationFactory {

    private val userDataRepository = UserDataRepositoryImpl(AppDatabase.getDatabase(context))

    override suspend fun getVibrationType(): ButtonVibrator {
        return if(userDataRepository.getUserdata()?.vibration!!){
            EnabledVibration(context)
        } else {
            DisabledVibration(context)
        }
    }
}