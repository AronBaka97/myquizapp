package world.travel.travelquiz.util.builder;

import java.util.ArrayList;
import java.util.List;

import world.travel.travelquiz.database.entities.Hint;

public class HintsListBuilder {

    public static List<Hint> generateHintsList() {
        List<Hint> hintsList = new ArrayList<>();
        hintsList.add(new Hint(0, "show_country", 1));
        hintsList.add(new Hint(0, "show_solution", 8));
        hintsList.add(new Hint(0, "show_information", 5));
        hintsList.add(new Hint(0, "show_flag", 2));
        return hintsList;
    }
}
