package world.travel.travelquiz.util.factory.vibration

interface VibrationFactory {

    suspend fun getVibrationType(): ButtonVibrator
}