package world.travel.travelquiz.util.builder;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import world.travel.travelquiz.R;
import world.travel.travelquiz.database.entities.Continent;

public class ContinentListBuilder {

    public static List<Continent> getContinentList(){
        List<Continent> continentList = new ArrayList<>();
        continentList.add(new Continent(0, R.drawable.europe, "Europe", Color.parseColor("#35AACB"), 0, 0));
        continentList.add(new Continent(0, R.drawable.na, "North America", Color.parseColor("#fae105"), 0, 0));
        continentList.add(new Continent(0, R.drawable.sa, "South America", Color.parseColor("#216F27"), 0, 0));
        continentList.add(new Continent(0, R.drawable.asia, "Asia", Color.parseColor("#471AA6"), 0, 0));
        continentList.add(new Continent(0, R.drawable.africa, "Africa", Color.parseColor("#fca103"), 0, 0));
        continentList.add(new Continent(0, R.drawable.aus, "Oceania", Color.parseColor("#FF0000"), 0, 0));
        return continentList;
    }
}
