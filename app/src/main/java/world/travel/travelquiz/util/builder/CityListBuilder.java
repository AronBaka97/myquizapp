package world.travel.travelquiz.util.builder;

import java.util.ArrayList;
import java.util.List;

import world.travel.travelquiz.database.entities.Quest;

import static world.travel.travelquiz.R.drawable.algiers;
import static world.travel.travelquiz.R.drawable.ankara;
import static world.travel.travelquiz.R.drawable.baires;
import static world.travel.travelquiz.R.drawable.baku;
import static world.travel.travelquiz.R.drawable.bangkok;
import static world.travel.travelquiz.R.drawable.barcelona;
import static world.travel.travelquiz.R.drawable.beijing;
import static world.travel.travelquiz.R.drawable.berlin;
import static world.travel.travelquiz.R.drawable.bogota;
import static world.travel.travelquiz.R.drawable.brasilia;
import static world.travel.travelquiz.R.drawable.bucharest;
import static world.travel.travelquiz.R.drawable.budapest;
import static world.travel.travelquiz.R.drawable.cairo;
import static world.travel.travelquiz.R.drawable.canberra;
import static world.travel.travelquiz.R.drawable.cape_town;
import static world.travel.travelquiz.R.drawable.chicago;
import static world.travel.travelquiz.R.drawable.cluj;
import static world.travel.travelquiz.R.drawable.delhi;
import static world.travel.travelquiz.R.drawable.dubai;
import static world.travel.travelquiz.R.drawable.havana;
import static world.travel.travelquiz.R.drawable.hobart;
import static world.travel.travelquiz.R.drawable.hongkong;
import static world.travel.travelquiz.R.drawable.istanbul;
import static world.travel.travelquiz.R.drawable.jerusalem;
import static world.travel.travelquiz.R.drawable.kuala_lumpur;
import static world.travel.travelquiz.R.drawable.la;
import static world.travel.travelquiz.R.drawable.lapaz;
import static world.travel.travelquiz.R.drawable.lima;
import static world.travel.travelquiz.R.drawable.lisbon;
import static world.travel.travelquiz.R.drawable.london;
import static world.travel.travelquiz.R.drawable.marrakesh;
import static world.travel.travelquiz.R.drawable.mecca;
import static world.travel.travelquiz.R.drawable.melbourne;
import static world.travel.travelquiz.R.drawable.mexico;
import static world.travel.travelquiz.R.drawable.montevideo;
import static world.travel.travelquiz.R.drawable.moscow;
import static world.travel.travelquiz.R.drawable.new_york;
import static world.travel.travelquiz.R.drawable.paris;
import static world.travel.travelquiz.R.drawable.perth;
import static world.travel.travelquiz.R.drawable.prague;
import static world.travel.travelquiz.R.drawable.pyongyang;
import static world.travel.travelquiz.R.drawable.quebec;
import static world.travel.travelquiz.R.drawable.rio;
import static world.travel.travelquiz.R.drawable.rome;
import static world.travel.travelquiz.R.drawable.santiago;
import static world.travel.travelquiz.R.drawable.seoul;
import static world.travel.travelquiz.R.drawable.sf;
import static world.travel.travelquiz.R.drawable.shanghai;
import static world.travel.travelquiz.R.drawable.sp;
import static world.travel.travelquiz.R.drawable.sydney;
import static world.travel.travelquiz.R.drawable.taipei;
import static world.travel.travelquiz.R.drawable.tokyo;
import static world.travel.travelquiz.R.drawable.toronto;
import static world.travel.travelquiz.R.drawable.tripoli;
import static world.travel.travelquiz.R.drawable.tunis;
import static world.travel.travelquiz.R.drawable.vancouver;
import static world.travel.travelquiz.R.drawable.washington;
import static world.travel.travelquiz.R.drawable.wellington;

/*
* ; separates the solution from extra options
*
* / breaks the solution into 2 lines on the UI
* for example: Washin/gton DC will be shown on the screen as:
*               ******
*               **** **
*
 */
public class CityListBuilder {

    private static final Integer CATEGORY_ID = 1;

    public static List<Quest> generateEuropeanCities() {
        List<Quest> europeanCityList = new ArrayList<>();
        europeanCityList.add(new Quest(0, london, "London;upirabrane", 1, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, paris, "Paris;eevuwiozocd", 1, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, berlin, "Berlin;orkautfroj", 1, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, budapest, "Budapest;ocekgrar", 1, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, barcelona, "Barcelona;lhepqvc", 1, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, prague, "Prague;rbailstjnm", 1, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, rome, "Rome;udankgkoddsz", 1, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, lisbon, "Lisbon;ikvbrnayir", 1, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, bucharest, "Bucharest;fklisko", 1, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        europeanCityList.add(new Quest(0, cluj, "Cluj-/Napoca;pfseeq", 1, 5, 0, false, CATEGORY_ID, new ArrayList<>()));
        return europeanCityList;
    }

    public static List<Quest> generateNorthAmericanCities() {
        List<Quest> naCityList = new ArrayList<>();
        naCityList.add(new Quest(0, la, "Los Angeles", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, new_york, "New York", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, toronto, "Toronto", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, chicago, "Chicago", 2, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, washington, "Washin/gton DC;elob", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, mexico, "Mexico/ City", 2, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, quebec, "Quebec", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, vancouver, "Vancouver", 2, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, sf, "San Francisco", 2, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        naCityList.add(new Quest(0, havana, "Havana", 2, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        return naCityList;
    }

    public static List<Quest> generateSouthAmericanCities() {
        List<Quest> saCityList = new ArrayList<>();
        saCityList.add(new Quest(0, bogota, "Bogota", 3, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, rio, "Rio de Janeiro", 3, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, brasilia, "Brasilia", 3, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, baires, "Buenos Aires", 3, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, montevideo, "Montevideo", 3, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, lapaz, "La Paz", 3, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, santiago, "Santiago", 3, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, lima, "Lima", 3, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        saCityList.add(new Quest(0, sp, "Sao Paulo", 3, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        return saCityList;
    }

    public static List<Quest> generateAsianCities() {
        List<Quest> asianCityList = new ArrayList<>();
        asianCityList.add(new Quest(0, moscow, "Moscow", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, beijing, "Beijing", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, tokyo, "Tokyo", 4, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, bangkok, "Bangkok", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, kuala_lumpur, "Kuala Lumpur", 4, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, taipei, "Taipei", 4, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, delhi, "New Delhi", 4, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, ankara, "Ankara", 4, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, istanbul, "Istanbul", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, dubai, "Dubai", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, seoul, "Seoul", 4, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, shanghai, "Shanghai", 4, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, hongkong, "Hong Kong", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, pyongyang, "Pyongyang", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, jerusalem, "Jerusalem", 4, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, mecca, "Mecca", 4, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        asianCityList.add(new Quest(0, baku, "Baku", 4, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        return asianCityList;
    }

    public static List<Quest> generateAfricanCities() {
        List<Quest> africanCityList = new ArrayList<>();
        africanCityList.add(new Quest(0, cairo, "Cairo", 5, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        africanCityList.add(new Quest(0, cape_town, "Cape Town", 5, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        africanCityList.add(new Quest(0, algiers, "Algiers", 5, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        africanCityList.add(new Quest(0, tripoli, "Tripoli", 5, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        africanCityList.add(new Quest(0, tunis, "Tunis", 5, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        africanCityList.add(new Quest(0, marrakesh, "Marrakech", 5, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        return africanCityList;
    }

    public static List<Quest> generateAustralianCities() {
        List<Quest> australianCityList = new ArrayList<>();
        australianCityList.add(new Quest(0, sydney, "Sydney", 6, 1, 0, false, CATEGORY_ID, new ArrayList<>()));
        australianCityList.add(new Quest(0, wellington, "Wellington", 6, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        australianCityList.add(new Quest(0, melbourne, "Melbourne", 6, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        australianCityList.add(new Quest(0, perth, "Perth", 6, 3, 0, false, CATEGORY_ID, new ArrayList<>()));
        australianCityList.add(new Quest(0, hobart, "Hobart", 6, 4, 0, false, CATEGORY_ID, new ArrayList<>()));
        australianCityList.add(new Quest(0, canberra, "Canberra", 6, 2, 0, false, CATEGORY_ID, new ArrayList<>()));
        return australianCityList;
    }


}
