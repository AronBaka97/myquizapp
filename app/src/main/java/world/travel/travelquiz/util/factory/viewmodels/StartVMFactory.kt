package world.travel.travelquiz.util.factory.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import world.travel.travelquiz.activities.start.StartViewModel
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.repository.impl.ContinentRepositoryImpl
import world.travel.travelquiz.repository.impl.HintRepositoryImpl
import world.travel.travelquiz.repository.impl.QuestRepositoryImpl
import world.travel.travelquiz.repository.impl.UserDataRepositoryImpl

class StartVMFactory(private val context: Context) : ViewModelProvider.Factory {

    private val appDataBase = AppDatabase.getDatabase(context)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return StartViewModel(
            QuestRepositoryImpl(appDataBase),
            ContinentRepositoryImpl(appDataBase),
            UserDataRepositoryImpl(appDataBase),
            HintRepositoryImpl(appDataBase)
        ) as T
    }
}