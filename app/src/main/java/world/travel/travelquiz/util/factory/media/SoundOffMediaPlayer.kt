package world.travel.travelquiz.util.factory.media

import android.content.Context
import world.travel.travelquiz.R

class SoundOffMediaPlayer(val context: Context): MediaPlayer {

    override fun playTone() {
        android.media.MediaPlayer.create(context, R.raw.correct_sound).also {
            it.setVolume(0.0f, 0.0f)
            it.start()
        }
    }
}