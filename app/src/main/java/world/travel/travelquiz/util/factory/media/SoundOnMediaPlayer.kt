package world.travel.travelquiz.util.factory.media

import android.content.Context
import world.travel.travelquiz.R.raw.correct_sound

class SoundOnMediaPlayer(val context: Context): MediaPlayer {

    override fun playTone() {
        android.media.MediaPlayer.create(context, correct_sound).also {
            it.setVolume(1.0f, 1.0f)
            it.start()
        }
    }
}