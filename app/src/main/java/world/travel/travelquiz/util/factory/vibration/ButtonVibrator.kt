package world.travel.travelquiz.util.factory.vibration

interface ButtonVibrator {

    fun vibrate()
}