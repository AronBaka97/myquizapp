package world.travel.travelquiz.util.factory.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import world.travel.travelquiz.activities.quiz.solver.QuestSolverViewModel
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.database.entities.Quest
import world.travel.travelquiz.repository.impl.HintRepositoryImpl
import world.travel.travelquiz.repository.impl.QuestRepositoryImpl
import world.travel.travelquiz.repository.impl.UserDataRepositoryImpl

class QuestSolverVMFactory(private val context: Context, private val quest: Quest) : ViewModelProvider.Factory {

    private val appDataBase = AppDatabase.getDatabase(context)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return QuestSolverViewModel(quest, QuestRepositoryImpl(appDataBase), UserDataRepositoryImpl(appDataBase), HintRepositoryImpl(appDataBase)) as T
    }
}