package world.travel.travelquiz.util.factory.media

import android.content.Context
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.repository.impl.UserDataRepositoryImpl

class MediaPlayerFactoryImpl(val context: Context): MediaPlayerFactory {

    private val userDataRepository = UserDataRepositoryImpl(AppDatabase.getDatabase(context))

    override suspend fun getMediaPlayer(): MediaPlayer {
        return if(userDataRepository.getUserdata()?.sound!!){
            SoundOnMediaPlayer(context)
        } else {
            SoundOffMediaPlayer(context)
        }
    }
}