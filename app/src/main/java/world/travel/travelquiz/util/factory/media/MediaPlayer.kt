package world.travel.travelquiz.util.factory.media

interface MediaPlayer {

    fun playTone()
}