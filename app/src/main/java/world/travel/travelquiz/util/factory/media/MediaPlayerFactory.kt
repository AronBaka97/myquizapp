package world.travel.travelquiz.util.factory.media

interface MediaPlayerFactory {

    suspend fun getMediaPlayer(): MediaPlayer

}