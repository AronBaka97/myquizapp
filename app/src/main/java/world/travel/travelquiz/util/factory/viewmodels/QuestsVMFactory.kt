package world.travel.travelquiz.util.factory.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import world.travel.travelquiz.activities.quiz.main.PageViewModel
import world.travel.travelquiz.database.AppDatabase
import world.travel.travelquiz.repository.impl.QuestRepositoryImpl

class QuestsVMFactory(context: Context) : ViewModelProvider.Factory {

    private val appDataBase = AppDatabase.getDatabase(context)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PageViewModel(QuestRepositoryImpl(appDataBase)) as T
    }
}