package world.travel.travelquiz.util.adapter

import android.util.DisplayMetrics
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions


@BindingAdapter("imageResource")
fun setImageResource(imageView: ImageView, resource: Int) {
    Glide.with(imageView.context)
        .load(resource)
        .apply(RequestOptions().centerCrop())
        .into(imageView)
}